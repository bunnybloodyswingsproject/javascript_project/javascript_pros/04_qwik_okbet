import { partytownSnippet } from "@builder.io/partytown/integration"
import AOS from "aos";
const snippetText = partytownSnippet();
const el = document.createElement("script");
el.innerText = snippetText;
document.body.appendChild(el);
// AOS animation
AOS.init();

// States
let lastScrollTop: number;

// Element variables
let navbar = document.querySelector(".navbar") as HTMLDivElement;
let closeNav = document.getElementById("closeNav") as HTMLElement;
let openNav = document.getElementById("openNav") as HTMLElement;
const learnMoreBtn = document.querySelector(".learnMoreBtn") as HTMLButtonElement;
const featureLink1 = document.getElementById("featureLink1") as HTMLAnchorElement;
const featureLink2 = document.getElementById("featureLink2") as HTMLAnchorElement;
const featureLink3 = document.getElementById("featureLink3") as HTMLAnchorElement;
const featureLink4 = document.getElementById("featureLink4") as HTMLAnchorElement;
const featureLink5 = document.getElementById("featureLink5") as HTMLAnchorElement;
const featureLink6 = document.getElementById("featureLink6") as HTMLAnchorElement;
const featureLink7 = document.getElementById("featureLink7") as HTMLAnchorElement;
const featureLink8 = document.getElementById("featureLink8") as HTMLAnchorElement;
const featureLinks = document.querySelector(".feature_nav_links") as HTMLDivElement;
const featureHeader = document.querySelector(".feature_description_header") as HTMLHeadingElement;
const featureNavDescription = document.querySelector(".feature_navigation_description") as HTMLDivElement;
const featureNavImg = document.querySelector(".featureNavImg") as HTMLImageElement;
const featureNavLinks = document.querySelectorAll(".featureNavLinks")!;

// Onscroll animation
document.querySelector('#main_header')!.scrollIntoView({ behavior: 'smooth' });
document.querySelector('#sports')!.scrollBy({
    top: -500, // could be negative value
    left: 0,
    behavior: 'smooth'
});
document.querySelector('#game_rules')!.scrollBy({
    top: -500, // could be negative value
    left: 0,
    behavior: 'smooth'
});


function featureElementsClassRemover() {
    featureNavLinks.forEach(link => {
        console.log(link.classList.contains("active"));
        if(link.classList.contains("active")) {
            link.classList.remove("active");
        }
    });
}

// Event Listeners
window.addEventListener("scroll", function() {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    if(scrollTop > lastScrollTop) {
        navbar.style.top='0';
    }else{
        navbar.style.top='0';
    }

    lastScrollTop = scrollTop;
});

openNav.addEventListener("click", function() {
    closeNav.classList.toggle("closeNavbar");
    closeNav.classList.remove("openNavbar");
    openNav.classList.remove("openNavbar");
    openNav.classList.toggle("closeNavbar");
    navbar.classList.toggle("openNavSlider");
})

closeNav.addEventListener("click", function() {
    openNav.classList.toggle("closeNavbar");
    openNav.classList.remove("openNavbar");
    closeNav.classList.remove("openNavbar");
    closeNav.classList.toggle("closeNavbar");
    navbar.classList.remove("openNavSlider");
});

learnMoreBtn.addEventListener("click", function () {
    featureLinks.classList.toggle("showFeatureNavlink");
});

featureLink1.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Basketball";
    featureNavDescription.style.backgroundColor = "#FFB549";
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/man_in_basketball-removebg-preview.png?alt=media&token=949c1341-085f-4e28-a5aa-f37b3b7ba53d";
    featureLink1.classList.toggle("active")
    featureLinks.classList.remove("showFeatureNavlink");
});

featureLink2.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Volleyball";
    featureNavDescription.style.backgroundColor = "linear-gradient(90deg, rgba(183, 0, 26, 1) 0%, rgba(255, 111, 79, 1) 100%);";
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/bolleybal-removebg-preview.png?alt=media&token=38e37a99-b835-4310-b635-8570791da92e";
    featureLink2.classList.toggle("active")
    featureLinks.classList.remove("showFeatureNavlink");
});

featureLink3.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Ice Hokey";
    featureNavDescription.style.backgroundColor = "#B7001A";
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/icehockey1.webp?alt=media&token=edee61e5-5bb8-4013-892c-e73a92044f93";
    featureLink3.classList.toggle("active")
    featureLinks.classList.remove("showFeatureNavlink");
});

featureLink4.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Boxing";
    featureNavDescription.style.backgroundColor = "#FFB549";
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/boxing_images-removebg-preview.png?alt=media&token=53ea4628-eac0-41fe-b76a-d3f0324b5fc9";
    featureLink4.classList.toggle("active")
    featureLinks.classList.remove("showFeatureNavlink");
});

featureLink5.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Rugby";
    featureNavDescription.style.backgroundColor = "#09178B";
    featureElementsClassRemover();
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/rugby-removebg-preview.png?alt=media&token=4904e057-b747-457d-923f-c640f6d41e48";
    featureLink5.classList.toggle("active")
    featureLinks.classList.remove("showFeatureNavlink");
});


featureLink6.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Mixed Martial Arts";
    featureNavDescription.style.backgroundColor = "#09178B";
    featureElementsClassRemover();
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/mma-removebg-preview.png?alt=media&token=9986b70f-7d3d-4e88-920e-697374970749";
    featureLink6.classList.toggle("active");
    featureLinks.classList.remove("showFeatureNavlink");
});


featureLink7.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Tennis";
    featureNavDescription.style.backgroundColor = "#009CD8";
    featureElementsClassRemover();
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/tennis-removebg-preview%20(1).png?alt=media&token=21bcb239-ef00-40ae-be42-b6549cdfce48";
    featureLink7.classList.toggle("active");
    featureLinks.classList.remove("showFeatureNavlink");
});

featureLink8.addEventListener("click", function () {
    featureElementsClassRemover();
    featureHeader.innerText = "Badminton";
    featureNavDescription.style.backgroundColor = "#FFB549";
    featureElementsClassRemover();
    featureNavImg.src = "https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/badminton-removebg-preview.png?alt=media&token=783b3760-2bc7-48d1-964b-3a341aeba25a";
    featureLink8.classList.toggle("active");
    featureLinks.classList.remove("showFeatureNavlink");
});