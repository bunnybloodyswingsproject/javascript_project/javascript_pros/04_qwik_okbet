const path = require("path");
module.exports = {
    mode: "development",
    entry: "./src/app.ts",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [{
            test: /\.ts$/,
            use: [
                {
                    loader: "ts-loader",
                    options: {
                        compilerOptions: {
                            "experimentalDecorators": true,
                            "moduleResolution": "node",
                            "noImplicitAny": true
                        }
                    }
                }
            ],
            include: [path.resolve(__dirname, "src")],
            exclude: /node_modules/
        }]
    },
    target: "web",
    devtool: "source-map",
    resolve: {
        extensions: [".ts", ".js"]
    },
    devServer: {
        static: "./dist"
    }
}